package scrape

type err struct {
	err error
}

type ErrRateConvert err

func (e ErrRateConvert) Error() string {
	return "scrape: failed to convert rate"
}

type ErrAmountConvert err

func (e ErrAmountConvert) Error() string {
	return "scrape: failed to convert amount"
}

type ErrTimeParse err

func (e ErrTimeParse) Error() string {
	return "scrape: failed to parse time"
}

type ErrPingTimeout err

func (e ErrPingTimeout) Error() string {
	return "scrape: pong message not received"
}

type ErrPongWrong err

func (e ErrPongWrong) Error() string {
	return "scrape: pong message incorrect"
}

type ErrConnWrite err

func (e ErrConnWrite) Error() string {
	return "scrape: failed to write to conn"
}

type ErrUnhandledMsg err

func (e ErrUnhandledMsg) Error() string {
	return "scrape: error message type received is unhandled"
}

type ErrApi err

func (e ErrApi) Error() string {
	return "scrape: received error message from api"
}

type ErrJson err

func (e ErrJson) Error() string {
	return "scrape: failed to marshal/unmarshal json"
}
