package scrape

import (
	"context"
	"net/url"

	"github.com/gorilla/websocket"
	"github.com/shopspring/decimal"

	"sync"
	"time"
)

type Controller struct {
	ctx context.Context
	// api url
	src *url.URL
	// processed trades received here
	out chan *Trade
	// request to initiate connection
	payload []byte
	// errors are received here
	errchan chan error
	// function ran to process trade
	processMsg func(*Controller, []byte)
	keepalive  time.Duration
	active     bool

	// base connection
	conn      *websocket.Conn
	writeLock *sync.Mutex
}

type Trade struct {
	Time     time.Time       `json:"time"`
	Pair     string          `json:"pair"`
	Rate     decimal.Decimal `json:"rate"`
	Amount   decimal.Decimal `json:"amount"`
	Exchange string
}

type DataPoint struct {
	Time    int             `json:"time"`
	Close   decimal.Decimal `json:"close"`
	High    decimal.Decimal `json:"high"`
	Low     decimal.Decimal `json:"low"`
	Open    decimal.Decimal `json:"open"`
	Volume1 decimal.Decimal `json:"volumefrom"`
	Volume2 decimal.Decimal `json:"volumeto"`
}
