package scrape

import (
	"errors"
	"testing"
)

func TestHandle(t *testing.T) {
	c := &Controller{}
	err := c.handleError(
		ErrRateConvert{
			errors.New("Error"),
		},
		"testing",
		123,
	)

	if err == nil {
		t.Error(err)
	}
}
