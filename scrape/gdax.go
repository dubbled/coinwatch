package scrape

import (
	"context"
	"encoding/json"
	"errors"
	"net/url"
	"time"

	"github.com/shopspring/decimal"
)

// https://docs.gdax.com
// Only suscribes to the matches channel
// Need to subscribe to heartbeat channel and make calls to REST API for all trades
func NewGDAX(ctx context.Context, pairs []string) (*Controller, error) {
	payload, err := json.Marshal(struct {
		Typ      string   `json:"type"`
		Pairs    []string `json:"product_ids"`
		Channels []string `json:"channels"`
	}{
		"subscribe",
		pairs,
		[]string{"matches"},
	})

	if err != nil {
		return nil, err
	}

	pm := func(c *Controller, msg []byte) {
		m := make(map[string]interface{})
		if err := json.Unmarshal(msg, &m); err != nil {
			c.errchan <- err
			return
		}

		switch m["type"] {
		case "last_match":
			fallthrough
		case "match":
			ti, err := time.Parse(time.RFC3339Nano, m["time"].(string))
			if err != nil {
				c.errchan <- c.handleError(ErrTimeParse{err}, m["time"].(string))
				return
			}

			rate, err := decimal.NewFromString(m["price"].(string))
			if err != nil {
				c.errchan <- c.handleError(ErrRateConvert{err}, m["rate"].(string))
				return
			}

			amount, err := decimal.NewFromString(m["size"].(string))
			if err != nil {
				c.errchan <- c.handleError(ErrAmountConvert{err}, m["size"].(string))
				return
			}

			if m["side"] == "sell" {
				amount = amount.Neg()
			}

			c.out <- &Trade{
				Pair:     m["product_id"].(string),
				Exchange: "GDAX",
				Time:     ti.Round(time.Second),
				Rate:     rate,
				Amount:   amount,
			}
		case "heartbeat":
			//
		case "error":
			c.errchan <- c.handleError(
				ErrApi{errors.New(
					m["message"].(string),
				)},
			)
		default:
		}
	}

	return NewController(
		ctx,
		&url.URL{Scheme: "wss", Host: "ws-feed.pro.coinbase.com", Path: "/"},
		payload,
		pm,
	), nil
}
