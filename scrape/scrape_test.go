package scrape

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestGDAX(t *testing.T) {
	ctx := context.Background()
	c, err := NewGDAX(ctx, []string{
		"ETH-USD", "BTC-USD",
	})
	if err != nil {
		t.Error(err)
	}

	if err = testController(c); err != nil {
		t.Error(err)
	}
}

func TestBFX(t *testing.T) {
	ctx := context.Background()
	c, err := NewBFX(ctx)
	if err != nil {
		t.Error(err)
	}

	if err = testController(c); err != nil {
		t.Error(err)
	}
}

func testController(c *Controller) error {
	if err := c.Start(); err != nil {
		return err
	}

	tick := time.NewTimer(time.Second * 10)
	if err := func() error {
		for {
			select {
			case <-tick.C:
				return nil
			case err := <-c.Errors():
				c.Terminate()
				return err
			case t := <-c.Out():
				fmt.Println(t)
			}
		}
	}(); err != nil {
		c.Terminate()
		return err
	}
	c.Terminate()
	return nil
}
