package scrape

import (
	"context"
	"encoding/json"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
	
	"github.com/shopspring/decimal"
)

// https://docs.bitfinex.com/v1/reference#ws-public-trades
// Subscribes to completed trades from Bitfinex
func NewBFX(ctx context.Context) (*Controller, error) {
	payload, err := json.Marshal(struct {
		Event   string `json:"event"`
		Channel string `json:"channel"`
		Symbol  string `json:"symbol"`
	}{
		"subscribe",
		"trades",
		"ETHUSD",
	})

	if err != nil {
		return nil, err
	}

	// matches trade messages from socket
	reg := regexp.MustCompile(`(\[\d+,"hb"\])|(\[\d+,\[(\[\d+,\d+,\-?\d+\.?\d*,\d+\.?\d*\]\,?)+\]\])|(\[([0-9]{1,5}),"([A-z]+)"),\[(-?\d+).\d+,(-?\d+).\d+,(-?\d+).\d+]]`)
	//channels := make(map[int]string)
	pm := func(c *Controller, msg []byte) {
		m := string(msg)
		if reg.MatchString(m) {
			fields := strings.Split(m, ",")
			if fields[1] == `"tu"` {
				epoch, err := strconv.ParseInt(fields[3], 10, 64)
				if err != nil {
					c.errchan <- c.handleError(ErrTimeParse{err}, fields[3])
					return
				}

				t := time.Unix(epoch/1000, 0)
				amount, err := decimal.NewFromString(fields[4])
				if err != nil {
					c.errchan <- c.handleError(ErrAmountConvert{err}, fields[4])
					return
				}

				p := fields[5][:len(fields[5])-2]
				price, err := decimal.NewFromString(p)
				if err != nil {
					c.errchan <- c.handleError(ErrRateConvert{err}, p)
					return
				}

				// tries := 0
				// for channels[int(fields[0][1:])] == "" {
				// 	time.Sleep(time.Second)
				// 	tries++
				// 	if tries > 5 {
				// 		return
				// 	}
				// }

				c.out <- &Trade{
					// Pair: 	  channels[int(fields[0][1:])],
					Pair: "ETHUSD",
					Exchange: "BFX",
					Time:     t.Round(time.Second),
					Rate:     price,
					Amount:   amount,
				}
			}
		} else {
			resp := make(map[string]interface{})
			var err error
			if err = json.Unmarshal(msg, &resp); err != nil {
				c.errchan <- c.handleError(ErrJson{err}, msg)
				return
			}

			switch resp["event"].(string) {
			case "error":
				c.errchan <- c.handleError(ErrApi{err}, "bfx api error")
				return
			case "subscribed":
				// channels[int(resp["chanId"].(float64))] = resp["pair"].(string)
			}

			// fmt.Println(channels)
		}
	}

	return NewController(
		ctx,
		&url.URL{Scheme: "wss", Host: "api.bitfinex.com", Path: "/ws/2"},
		payload,
		pm,
	), nil
}
