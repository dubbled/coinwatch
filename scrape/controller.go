package scrape

import (
	"bytes"
	"context"
	"fmt"
	"net/url"

	"github.com/gorilla/websocket"

	"sync"
	"time"
)

func NewController(ctx context.Context, src *url.URL, payload []byte, processMsg func(*Controller, []byte)) *Controller {
	return &Controller{
		ctx:        ctx,
		src:        src,
		payload:    payload,
		processMsg: processMsg,
		out:        make(chan *Trade, 100),
		errchan:    make(chan error, 10),
	}
}

func (c *Controller) Start() error {
	conn, _, err := websocket.DefaultDialer.Dial(c.src.String(), nil)
	if err != nil {
		return err
	}

	c.writeLock = &sync.Mutex{}
	c.conn = conn
	c.active = true

	if c.keepalive == 0 {
		c.keepalive = time.Duration(5)
	}

	if c.payload != nil {
		if err = c.Write(websocket.TextMessage, c.payload); err != nil {
			return err
		}
	}

	go func() {
		defer func() {
			if err := c.conn.Close(); err != nil {
				c.errchan <- err
			}
			c.active = false
		}()

		timer := time.NewTimer(c.keepalive)
		pong := make(chan []byte, 1)

	Loop:
		for {
			select {
			case <-c.ctx.Done():
				break Loop
			case <-timer.C:
				go func() {
					pingMsg := []byte(time.Now().String())
					if err = c.Write(websocket.PingMessage, pingMsg); err != nil {
						c.errchan <- c.handleError(ErrConnWrite{})
						c.Terminate()
					}

					timer.Reset(c.keepalive)
					select {
					case <-timer.C:
						c.errchan <- c.handleError(ErrPingTimeout{})

					case pongMsg := <-pong:
						if !bytes.Equal(pingMsg, pongMsg) {
							c.errchan <- c.handleError(ErrPongWrong{}, pingMsg, pongMsg)
						}
					}
				}()
			default:
			}

			mt, msg, err := c.conn.ReadMessage()
			if err != nil {
				c.errchan <- err
				return
			}

			go func() {
				switch mt {
				case websocket.PingMessage:
					if err = c.Write(websocket.PongMessage, msg); err != nil {
						c.errchan <- c.handleError(ErrConnWrite{})
						return
					}
				case websocket.PongMessage:
					pong <- msg
				case websocket.TextMessage:
					fmt.Println(string(msg))
					c.processMsg(c, msg)
				default:
					c.errchan <- c.handleError(ErrUnhandledMsg{})
				}
			}()
		}
	}()

	return c.conn.Close()
}

func (c *Controller) Write(typ int, msg []byte) error {
	c.writeLock.Lock()
	err := c.conn.WriteMessage(typ, msg)
	c.writeLock.Unlock()
	return err
}

func (c *Controller) Out() chan *Trade {
	return c.out
}

func (c *Controller) Errors() chan error {
	return c.errchan
}

func (c *Controller) Terminate() {
	ctx, cancel := context.WithCancel(c.ctx)
	c.ctx = ctx
	cancel()
}
