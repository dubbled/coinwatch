package scrape

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
)

func NewCoinApi(ctx context.Context, pairs []string) (*Controller, error) {
	var payload []byte
	var err error
	if payload, err = json.MarshalIndent(struct {
		Typ       string   `json:"type"`
		ApiKey    string   `json:"apikey"`
		Heartbeat bool     `json:"heartbeat"`
		DataType  []string `json:"subscribe_data_type"`
		Assets    []string `json:"subscribe_filter_asset_id"`
	}{
		"hello",
		//"081A69C5-2E71-409F-96C9-C51673A0825A",
		"",
		true,
		[]string{"trade", "quote"},
		pairs,
	}, "", "\t"); err != nil {
		return nil, err
	}
	fmt.Println(string(payload))
	pm := func(c *Controller, msg []byte) {
		fmt.Println(string(msg))

	}

	return NewController(
		ctx,
		&url.URL{Scheme: "wss", Host: "ws.coinapi.io", Path: "/v1"},
		payload,
		pm,
	), nil
}
