package scrape

import (
	"fmt"
	"github.com/pkg/errors"
)

func (c *Controller) handleError(err error, info ...interface{}) error {
	switch err.(type) {
	case ErrConnWrite:
		info = append(info, c.src)
	}

	var str string
	if info != nil {
		for i, v := range info {
			if i < len(info) - 1{
				str = fmt.Sprintf("%s [%#v], ", str, v)
			} else {
				str = fmt.Sprintf("%s [%#v]", str, v)
			}
		}
	}

	return errors.Wrap(err, str)
}
