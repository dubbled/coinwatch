package main

import (
	"context"
	// "time"
	"fmt"
	"os"
	"os/signal"

	"errl/coin/api"
	"errl/coin/scrape"
	// "errl/coin/db"
	"errl/coin/db/sqlite"

	// "github.com/fatih/color"
	// "github.com/shopspring/decimal"
)

func main() {
	ctx := context.Background()
	cctx, cancel := context.WithCancel(ctx)
	kill := make(chan os.Signal, 2)

	signal.Notify(kill, os.Interrupt)

	gdax, err := scrape.NewGDAX(cctx)
	if err != nil {
		panic(err)
	}

	sq, err := sqlite.New("./lol.db", false)
	if err != nil {
		panic(err)
	}

	app := api.API{}
	if err := app.Start(map[string]*scrape.Controller{
		"gdax": gdax,
	}, sq); err != nil {
		panic(err)
	}

	select {
	case <-kill:
		fmt.Println("\nExiting gracefully...")
		cancel()
		app.Terminate()
		sq.Close()
		return
	}
}
