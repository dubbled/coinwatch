package db

import (
	"time"

	"github.com/dubbled/coin/scrape"

	_ "github.com/lib/pq"
	"github.com/shopspring/decimal"
)

type DB interface {
	InsertTrade(*scrape.Trade) error
	TradeCount(pair string, t1, t2 time.Time) (buys, sells []TradeCount, err error)
	TradeVolume(pair string, t1, t2 time.Time) (buys, sells decimal.Decimal, err error)
	//CountPerSecond(pair string, t1, t2 time.Time) (buys, sells []TradeCount, err error)
	AverageRate(pair string, t1, t2 time.Time) (buys, sells []TradeRate, err error)
	AvgRate(pair string, t1, t2 time.Time) (out TradeRate, err error)
	AverageAmount(pair string, t1, t2 time.Time) (buys, sells []TradeAmount, err error)

	Trades(pair string, t1, t2 time.Time) ([]scrape.Trade, error)
	AllTrades(pair string) ([]scrape.Trade, error)
}

type TradeRate struct {
	Time time.Time       `json:"time"`
	Rate decimal.Decimal `json:"rate"`
}

type TradeCount struct {
	Time  time.Time `json:"time"`
	Count int64     `json:"count"`
}

type TradeAmount struct {
	Time   time.Time       `json:"time"`
	Amount decimal.Decimal `json:"amount"`
}

type TradeVolume struct {
	Time   time.Time       `json:"time"`
	Volume decimal.Decimal `json:"volume"`
}
