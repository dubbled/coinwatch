package sqlite

import (
	"database/sql"
	"fmt"
	"time"

	"errl/coin/db"
	"errl/coin/scrape"

	"errors"

	_ "github.com/mattn/go-sqlite3"
	"github.com/shopspring/decimal"
)

var tables = []string{
	`create table if not exists crypto (
		time timestamp,
		fsym text,
		tsym text,
		open text,
		close text,
		high text,
		low text,
		volume1 text,
		volume2 text,
		range text,
		differential text,
		volumedelta text
	);`,
	`create table if not exists trades (
		time timestamp,
		pair text,
		amount text,
		rate text,
		exchange text
	);`,
}

type Sqlite struct {
	*sql.DB
	Tables []string
}

func New(path string, mem bool) (sqlite *Sqlite, err error) {
	if mem {
		path = "file::memory:?mode=memory&cache=shared&mode=rwc&journal_mode=WAL"
	} else {
		path = fmt.Sprintf("file:%s?loc=auto&cache=shared&mode=rwc&journal_mode=WAL",
			path,
		)
	}

	db, err := sql.Open("sqlite3", path)
	if err != nil || err = db.Ping(); err != nil {
		return
	}

	db.SetMaxOpenConns(1)

	var s *sql.Stmt
	for _, k := range tables {
		s, err = db.Prepare(k)
		if err != nil {
			return
		}
		_, err = s.Exec()
		if err != nil {
			return
		}
	}

	sqlite = &Sqlite{
		db,
		tables,
	}

	return
}

func (sq *Sqlite) InsertTrade(trade *scrape.Trade) (err error) {
	_, err = sq.Exec(`
	insert into trades
		(time, pair, amount, rate, exchange)
	values
		(?, ?, ?, ?, ?)
	`,
		trade.Time.Truncate(time.Second),
		trade.Pair,
		trade.Amount,
		trade.Rate,
		trade.Exchange,
	)

	return
}

func (sq *Sqlite) AverageRate(pair string, t1, t2 time.Time) (buys, sells []db.TradeRate, err error) {
	d := db.TimeRangeSeconds(t2, t1)
	scan := func(r *sql.Rows) ([]db.TradeRate, error) {
		out := make([]db.TradeRate, 0, d)
		for r.Next() {
			tr := db.TradeRate{}
			if err = r.Scan(&tr.Time, &tr.Rate); err != nil {
				return nil, err
			}
			tr.Rate = tr.Rate.Round(2)
			out = append(out, tr)
		}
		return out, nil
	}

	r, err := sq.Query(`
		select
			time, avg(rate)
		from
			trades
		where
			pair = (?) and
			time >= Datetime(?) and
			time <= Datetime(?) and
			amount < 0
		group by
			time
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return
	}

	sells, err = scan(r)

	r, err = sq.Query(`
		select
			time, avg(rate)
		from
			trades
		where
			pair = (?) and
			time >= Datetime(?) and
			time <= Datetime(?) and
			amount > 0
		group by
			time
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	buys, err = scan(r)

	return
}

func (sq *Sqlite) AvgRate(pair string, t1, t2 time.Time) (db.TradeRate, error) {
	// d := db.TimeRangeSeconds(t2, t1)
	scan := func(r *sql.Rows) (db.TradeRate, error) {
		tr := db.TradeRate{}
		rate := decimal.Decimal{}
		for r.Next() {
			if err := r.Scan(&rate); err != nil {
				return db.TradeRate{}, err
			}
		}

		rate = rate.Round(2)
		tr.Rate = rate
		tr.Time = t2
		fmt.Println(tr)
		return tr, nil
	}

	r, err := sq.Query(`
		select
			avg(rate)
		from
			trades
		where
			pair = ? and
			time >= Datetime(?) and
			time <= Datetime(?)
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return db.TradeRate{}, nil
	}

	out, err := scan(r)
	return out, nil
}

func (sq *Sqlite) TradeCount(pair string, t1, t2 time.Time) (buys, sells []db.TradeCount, err error) {
	d := db.TimeRangeSeconds(t2, t1)
	if d < 0 {
		return nil, nil, errors.New("invalid time range")
	}
	scan := func(r *sql.Rows) ([]db.TradeCount, error) {
		out := []db.TradeCount{}
		for r.Next() {
			tc := db.TradeCount{}
			if err = r.Scan(&tc.Time, &tc.Count); err != nil {
				return nil, err
			}
			out = append(out, tc)
		}

		return out, nil
	}

	r, err := sq.Query(`
		select
			time, count(*)
		from
			trades
		where
			pair = ? and
			time >= Datetime(?) and
			time <= Datetime(?) and
			amount > 0
		group by
			time
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return nil, nil, err
	}

	buys, err = scan(r)

	r, err = sq.Query(`
		select
			time, count(*)
		from
			trades
		where
			pair = ? and
			time >= Datetime(?) and
			time <= Datetime(?) and
			amount < 0
		group by
			time
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return nil, nil, err
	}

	sells, err = scan(r)

	return
}

func (sq *Sqlite) TradeVolume(pair string, t1, t2 time.Time) (buys, sells decimal.Decimal, err error) {
	scan := func(r *sql.Rows, d *decimal.Decimal) {
		for r.Next() {
			var amt decimal.Decimal
			var rate decimal.Decimal
			if err = r.Scan(&amt, &rate); err != nil {
				continue
			}

			*d = d.Add(amt.Mul(rate))
		}
	}

	r, err := sq.Query(`
		select
			amount, rate
		from
			trades
		where
			amount < 0 and
			time >= Datetime(?) and
			time <= Datetime(?)
	`, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return
	}

	scan(r, &buys)

	r, err = sq.Query(`
		select
			amount, rate
		from
			trades
		where
			amount > 0 and
			time >= Datetime(?) and
			time <= Datetime(?)
	`, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return
	}

	scan(r, &sells)

	sells = sells.Round(2)
	buys = buys.Round(2)
	return
}

func (sq *Sqlite) AverageAmount(pair string, t1, t2 time.Time) (buys, sells []db.TradeAmount, err error) {
	scan := func(r *sql.Rows, out []db.TradeAmount) {
		d := t2.Sub(t1).Seconds()
		out = make([]db.TradeAmount, int(d))
		for r.Next() {
			t := db.TradeAmount{}
			if err = r.Scan(&t.Time, &t.Amount); err != nil {
				continue
			}

			out = append(out, t)
		}
	}

	r, err := sq.Query(`
		select
			time, avg(amount)
		from
			trades
		where
			pair = ? and
			time >= Datetime(?) and
			time <= Datetime(?) and
			amount < 0
		group by
			time
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return
	}

	scan(r, sells)

	r, err = sq.Query(`
		select
			time, avg(amount)
		from
			trades
		where
			pair = ? and
			time >= Datetime(?) and
			time <= Datetime(?) and
			amount > 0
		group by
			time
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return
	}

	scan(r, buys)

	return
}

func (sq *Sqlite) Trades(pair string, t1, t2 time.Time) (trades []scrape.Trade, err error) {
	r, err := sq.Query(`
		select time, rate, amount from trades where pair = ? and time >= ? and time <= ? limit 100
	`, pair, db.FormatDatetime(t1), db.FormatDatetime(t2))

	if err != nil {
		return
	}

	trades = make([]scrape.Trade, 0, 100)
	for r.Next() {
		t := scrape.Trade{}
		if err = r.Scan(&t.Time, &t.Rate, &t.Amount); err != nil {
			return
		}

		trades = append(trades, t)
	}

	return
}

func (sq *Sqlite) AllTrades(pair string) (trades []scrape.Trade, err error) {
	r, err := sq.Query(`select time, rate, amount from trades`)
	if err != nil {
		return nil, err
	}

	trades = make([]scrape.Trade, 0, 100)
	for r.Next() {
		t := scrape.Trade{}
		if err = r.Scan(&t.Time, &t.Rate, &t.Amount); err != nil {
			return
		}

		trades = append(trades, t)
	}

	return
}
