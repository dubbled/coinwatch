package sqlite

import (
	"fmt"
	"testing"
	"time"

	"errl/coin/scrape"

	"github.com/shopspring/decimal"
)

func openDB() (*Sqlite, error) {
	db, err := New("test.db", true)
	if err != nil {
		return &Sqlite{}, err
	}

	return db, nil
}

func TestInsertTrades(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Error(err)
	}

	var i int64
	timestamp := time.Now()
	for i = 0; i < 100; i++ {
		trade := scrape.Trade{}
		trade.Rate = decimal.NewFromFloat(324.241 - float64(i))
		trade.Pair = "ETHUSD"
		trade.Amount = decimal.New(i, 0)
		if i%2 == 0 {
			trade.Amount = trade.Amount.Neg()
		}

		if i%5 == 0 {
			timestamp = timestamp.Add(-1 * time.Second)
		}

		trade.Time = timestamp
		if err := db.InsertTrade(&trade); err != nil {
			t.Error(err)
		}
	}

	trades, err := db.Trades(
		"ETHUSD",
		time.Now().Add(-time.Hour*30000),
		time.Now(),
	)

	if err != nil {
		t.Error(err)
	}
	fmt.Printf("Trade Count: %d\n", len(trades))
}

func clearTrades(db Sqlite) error {
	_, err := db.Exec("delete from trades")
	return err
}

func TestTrades(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Error(err)
	}

	trades, err := db.Trades(
		"ETHUSD",
		time.Now().Add(-time.Hour*3000),
		time.Now(),
	)

	if err != nil {
		t.Error(err)
	}

	fmt.Printf("# of Trades: %d\n", len(trades))
}

func TestAverageRate(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Error(err)
	}

	b, s, err := db.AverageRate(
		"ETHUSD",
		time.Now().Add(time.Hour*-1),
		time.Now(),
	)

	if err != nil {
		t.Error(err)
	}

	fmt.Printf("Calculated %d averages for buy orders.\n", len(b))
	fmt.Printf("Calculated %d averages for sels orders.\n", len(s))
}

func TestAvgRate(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Error(err)
	}

	avg, err := db.AvgRate(
		"ETHUSD",
		time.Now().Add(time.Hour*-1),
		time.Now(),
	)

	if err != nil {
		t.Error(err)
	}

	fmt.Println(avg)
}

func TestTradeCount(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Error(err)
	}

	b, s, err := db.TradeCount(
		"ETHUSD",
		time.Now().Add(-time.Hour),
		time.Now(),
	)
	if err != nil {
		t.Error(err)
	}

	fmt.Printf("Buys: %d, Sells: %d\n", len(b), len(s))
}

func TestTradeVolume(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Error(err)
	}

	buys, sells, err := db.TradeVolume(
		"ETHUSD",
		time.Now().Add(-time.Hour*10000),
		time.Now(),
	)

	if err != nil {
		t.Error(err)
	}

	fmt.Printf("Buy: %s, Sells: %s\n", buys, sells)
}
