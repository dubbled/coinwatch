package db

import (
	"fmt"
	"strings"
	"testing"
	"time"
)

func TestFormatDatetime(t *testing.T) {
	s := FormatDatetime(time.Now())
	s = strings.Replace(s, "-", " ", -1)
	s = strings.Replace(s, "T", " ", 1)
	fmt.Println(s)
}
