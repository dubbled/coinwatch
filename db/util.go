package db

import (
	"strings"
	"time"
)

func TimeRangeSeconds(t1, t2 time.Time) int64 {
	return int64(t1.Sub(t2).Seconds())
}

func FormatDatetime(t time.Time) string {
	t = t.Truncate(time.Second)
	ti := t.Format(time.RFC3339)
	index := strings.LastIndexAny(ti, "-")
	return ti[:index]
}
