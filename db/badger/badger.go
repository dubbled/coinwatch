package badger

import (``
	"encoding/json"
	"fmt"

	"errl/coin/scrape"

	"github.com/dgraph-io/badger"
	"github.com/google/uuid"
)

type Badger struct {
	*badger.DB
}

func New(path string) (*Badger, error) {
	opts := badger.DefaultOptions
	opts.Dir = path
	opts.ValueDir = path
	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	b := &Badger{
		db,
	}

	return b, nil
}

func (db *Badger) InsertTrade(trade *scrape.Trade) (err error) {
	id := []byte(uuid.New().String())
	b, err := json.Marshal(trade)
	if err != nil {
		return
	}

	err = db.Update(func(txn *badger.Txn) error {
		return txn.Set(id, b)
	})

	return
}

func (db *Badger) GetTrades() (trades []scrape.Trade, err error) {
	trades = make([]scrape.Trade, 10)
	err = db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false

		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			fmt.Println(k)
			z, err := txn.Get(k)
			if err != nil {
				return err
			}
			fmt.Println(z.String())
		}
		return nil
	})

	return
}
