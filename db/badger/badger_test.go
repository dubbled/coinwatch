package badger

import (
	"errl/coin/scrape"
	"path/filepath"
	"testing"
	"time"

	"github.com/shopspring/decimal"
)

func TestTrades(t *testing.T) {
	path := filepath.Clean("C:/Users/Asa/go/badger")
	db, err := New(path)
	if err != nil {
		t.Error(err)
	}

	t1 := &scrape.Trade{
		Time:   time.Now(),
		Pair:   "ETHUSD",
		Rate:   decimal.NewFromFloat(300.3),
		Amount: decimal.NewFromFloat(1.3),
	}

	if err = db.InsertTrade(t1); err != nil {
		t.Error(err)
	}

	_, err = db.GetTrades()
	if err != nil {
		t.Error(err)
	}

	//fmt.Println(trades)
}
