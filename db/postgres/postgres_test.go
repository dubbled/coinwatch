package postgres

import (
	"time"
	"testing"

	"fmt"
)

//func TestInsert(t *testing.T) {
//	ps, err := New()
//	if err != nil {
//		t.Error(err)
//	}
//
//	if err = insertTrades(ps); err != nil {
//		t.Error(err)
//	}
//}
//
//func insertTrades(db db.DB) error {
//	var i int64
//	timestamp := time.Now()
//	for i = 0; i < 100; i++ {
//		t := scrape.Trade{}
//		t.Rate = decimal.NewFromFloat(324.241 - float64(i))
//		t.Pair = "ETHUSD"
//		t.Amount = decimal.New(i, 0)
//		if i%2 == 0 {
//			t.Amount = t.Amount.Neg()
//		}
//
//		if i%5 == 0 {
//			timestamp = timestamp.Add(time.Duration(-1) * time.Second)
//		}
//
//		t.Time = timestamp
//		if err := db.InsertTrade(&t); err != nil {
//			return err
//		}
//	}
//
//	return nil
//}

func TestTradeCount(t *testing.T) {
	ps, err := New()
	if err != nil {
		t.Error(err)
	}

	buys, sells, err := ps.TradeCount("ETHUSD", time.Now(), time.Now().Add(-time.Hour))
	if err != nil {
		t.Error(err)
	}

	fmt.Println(buys)
	fmt.Println(sells)
}