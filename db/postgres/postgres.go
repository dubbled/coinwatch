package postgres

import (
	"database/sql"
	"fmt"

	"errl/coin/db"
	"errl/coin/scrape"

	"time"

	_ "github.com/lib/pq"
	"github.com/shopspring/decimal"
)

var tables = []string{
	`
	create table if not exists crypto
	(
		time timestamp NOT NULL,
		fsym varchar(3) NOT NULL,
		tsym varchar(3) NOT NULL,
		open numeric(15, 2),
		close numeric(15, 2),
		high numeric(15, 2),
		low numeric(15, 2),
		volume1 numeric(15, 2),
		volume2 numeric(15, 2),
		range numeric(15, 2),
		differential numeric(15, 2),
		volumedelta numeric(15, 2)
	);
	`,
	`
	create table if not exists trades
	(
		time timestamp,
		pair varchar(6),
		amount numeric(10, 5),
		rate numeric(7, 2),
		exchange varchar(9)
	)`,
}

type Psql struct {
	*sql.DB
	Tables []string
}

func New() (psql *Psql, err error) {
	conn_str := fmt.Sprintf(`
		host=%s 
		port=%d 
		user=%s 
		dbname=%s 
		sslmode=disable`,
		"192.168.69.106", 5432, "postgres", "crypto")

	db, err := sql.Open("postgres", conn_str)
	if err != nil {
		return
	}

	if err = db.Ping(); err != nil {
		return
	}

	for _, k := range tables {
		if _, err = db.Query(k); err != nil {
			return
		}
	}

	psql = &Psql{
		db,
		tables,
	}

	return
}

func (ps *Psql) InsertTrade(trade *scrape.Trade) (err error) {
	_, err = ps.Exec(`
	insert into trades
	  (time, pair, amount, rate, exchange)
	values
	  ($1, $2, $3, $4, $5)`,
		trade.Time.Truncate(time.Second), trade.Pair, trade.Amount, trade.Rate, trade.Exchange)

	return
}

func (ps *Psql) TradeCount(pair string, t1, t2 time.Time) (buys, sells []db.TradeCount, err error) {
	scan := func(r *sql.Rows) []db.TradeCount {
		out := []db.TradeCount{}
		for r.Next() {
			tc := db.TradeCount{}
			if err := r.Scan(&tc.Time, &tc.Count); err != nil {
				continue
			}
			out = append(out, tc)
		}
		return out
	}

	r, err := ps.Query(`
		select
			time, count(*)
		from
		  	trades
		where
		  	pair = $1 and
		  	time >= $2 and
		  	time <= $3 and
		  	amount > 0
		group by
			time
		order by
		  	time asc
	`, pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	buys = scan(r)

	r, err = ps.Query(`
		select 
			time, count(*)
		from
			trades
		where
			pair = $1 and 
			time >= $2 and
			time <= $3 and 
			amount < 0
		group by
		  	time	
	`, pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	sells = scan(r)

	return
}

func (ps *Psql) TradeVolume(pair string, t1, t2 time.Time) (buys, sells decimal.Decimal, err error) {
	scan := func(r *sql.Rows, d decimal.Decimal) {
		for r.Next() {
			v := decimal.Decimal{}
			if err = r.Scan(&v); err != nil {
				continue
			}
			d.Add(v)
		}
	}

	r, err := ps.Query(`
		select
			rate * amount
		from
	  		trades
	  	where
	  		pair = $1 and
	  		time >= $2 and
	  		time <= $3 and
	  		amount < 0`, pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	scan(r, sells)

	r, err = ps.Query(`
		select
			rate * amount
		from
	  		trades
	  	where
	  		pair = $1 and
	  		time >= $2 and
	  		time <= $3 and
	  		amount < 0`, pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	scan(r, buys)

	return
}

func (ps *Psql) AverageRate(pair string, t1, t2 time.Time) (buys, sells []db.TradeRate, err error) {
	scan := func(r *sql.Rows) []db.TradeRate {
		out := []db.TradeRate{}
		for r.Next() {
			tr := db.TradeRate{}
			if err = r.Scan(&tr.Time, &tr.Rate); err != nil {
				continue
			}
			out = append(out, tr)
		}
		return out
	}

	r, err := ps.Query(`
		select
			time, rate
		from
			trades
		where
			pair = $1 and
			time >= $2 and
			time <= $3 and
			amount < 0`,
		pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	sells = scan(r)

	r, err = ps.Query(`
		select
			time, rate
		from
			trades
		where
			pair = $1 and
			time >= $2 and
			time <= $3 and
			amount > 0`,
		pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	buys = scan(r)

	return
}

func (ps *Psql) AverageAmount(pair string, t1, t2 time.Time) (buys, sells []db.TradeAmount, err error) {
	scan := func(r *sql.Rows) []db.TradeAmount {
		out := []db.TradeAmount{}
		for r.Next() {
			ta := db.TradeAmount{}
			if err = r.Scan(&ta.Time, &ta.Amount); err != nil {
				continue
			}
			fmt.Println(ta)
			out = append(out, ta)
		}
		return out
	}

	r, err := ps.Query(`
		select
			time, amount
		from
			trades
		where
			pair = $1 and
			time >= $2 and
			time <= $3 and
			amount < 0`,
		pair, t1, t2)

	if err != nil {
		return
	}

	sells = scan(r)

	r, err = ps.Query(`
		select
			time, amount
		from
			trades
		where
			pair = $1 and
			time >= $2 and
			time <= $3 and
			amount > 0`,
		pair, t1.Format(time.RFC3339), t2.Format(time.RFC3339))

	if err != nil {
		return
	}

	buys = scan(r)

	return
}
