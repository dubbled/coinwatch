package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"errl/coin/db"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		fmt.Println(r.Header["Origin"])
		return true
	},
	Subprotocols: []string{"trades"},
}

type API struct {
	ctx         context.Context
	Debug       bool
	Logger      io.Writer
	db          db.DB
	errchan     chan error
	port        string
}

func New(ctx context.Context) *API {
	api := &API{
		ctx:     ctx,
		Debug:   true,
		errchan: make(chan error),
		port:    ":9090",
	}

	return api
}

func (api *API) Start(db db.DB) error {
	api.errchan = make(chan error)
	api.db = db

	r := chi.NewRouter()
	r.Route("/", func(r chi.Router) {
		r.Get("/trades", api.TradeSocket())
		r.Get("/avgrate", api.AverageRate())
	})

	srv := &http.Server{Addr: api.port, Handler: r}
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			api.errchan <- err
		}
	}()

	go func() {
		for err := range api.errchan {
			if api.Debug == true {
				fmt.Printf("Error: %s\n", err)
			}
		}
	}()

	go func() {
		select {
		case <-api.ctx.Done():
			srv.Shutdown(context.TODO())
		}
	}()

	return nil
}

func (api *API) Errors() chan error {
	return api.errchan
}

func (api *API) TradeSocket() func(http.ResponseWriter, *http.Request) {
	updateRate := 5
	return func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}

		ctx := context.Background()

		defer conn.Close()
		ticker := time.NewTimer(time.Second * 3)
		ping := make(chan []byte)

	Loop:
		for {
			select {
			case <-ctx.Done():
				break Loop
			case msg := <-ping:
				if err := conn.WriteMessage(websocket.PongMessage, msg); err != nil {
					api.errchan <- err
				}
			case <-ticker.C:
				t1 := time.Now().Add(-time.Second * time.Duration(updateRate+1)).Truncate(time.Second)
				t2 := time.Now().Add(-time.Second).Truncate(time.Second)
				fmt.Println(t1)
				fmt.Println(t2)
				buys, sells, err := api.db.TradeCount(
					"ETHUSD",
					t2,
					t1,
				)
				if err != nil {
					api.errchan <- err
					continue
				}
				resp := map[string][]db.TradeCount{
					"buys":  buys,
					"sells": sells,
				}
				body, err := json.Marshal(resp)
				if err != nil {
					api.errchan <- err
					return
				}

				if err := conn.WriteMessage(websocket.TextMessage, body); err != nil {
					api.errchan <- err
					return
				}

				ticker.Reset(time.Second * time.Duration(updateRate))
			}
		}
	}
}

func (api *API) AverageRate() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		_,_ , err := api.db.AverageRate(
			"ETHUSD",
			time.Now().Truncate(1*time.Hour),
			time.Now(),
		)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to get average rate"))
			fmt.Println(err)
			return
		}

		w.Write([]byte("test"))
	}
}

func (api *API) TradeRatio() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		dec := json.NewDecoder(r.Body)
		m := make(map[string]interface{})
		if err := dec.Decode(&m); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("failed to decode request"))
			return
		}

		defer r.Body.Close()
		w.Write([]byte("Testing"))
	}
}
