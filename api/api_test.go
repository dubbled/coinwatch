package api

import (
	"testing"
	"context"
	"errl/coin/db/sqlite"
	//"github.com/gorilla/websocket"
	//"net/http"
	//"fmt"
	//"time"
	"github.com/gorilla/websocket"
	"net/http"
	"fmt"
	"time"
)

func TestAPI_Start(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	sq, err := sqlite.New("./test.db", true)
	if err != nil {
		t.Error(err)
	}

	api := New(ctx)
	if err = api.Start(sq); err != nil {
		t.Error(err)
	}

	conn, _, err := websocket.DefaultDialer.Dial("ws://localhost:9090/trades", http.Header{
		"Sec-WebSocket-Protocol": []string{"trades"},
	})

	if err != nil {
		t.Error(err)
	}

	go func() {
		select {
		case err = <-api.Errors():
			t.Error(err)
		case <-api.ctx.Done():
			return
		}
	}()

	in := make(chan []byte, 5)

	go func() {
		for {
			if conn != nil {
				_, msg, err := conn.ReadMessage()
				if err != nil {
					t.Error(err)
				}
				in <- msg
			} else {
				return
			}
		}
	}()

	ticker := time.NewTicker(time.Second * 5)
	func() {
		for {
			select {
			case msg := <-in:
				fmt.Println(msg)
			case <-ticker.C:
				cancel()
				time.Sleep(time.Second)
				conn = nil
				return
			}
		}
	}()
}
